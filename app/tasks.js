const express = require('express');
const auth = require('../middleware/auth');
const Task = require('../models/Task');

const createRouter = () => {
    const router = express.Router();

    router.post('/', auth, (req, res) => {
        const task = new Task(req.body);

        task.user = req.user._id;

        task.save()
            .then(task => res.send(task))
            .catch(error => res.status(400).send(error));
    });

    router.get('/', auth, (req, res) => {
        Task.find({user: req.user._id})
            .then(tasks => res.send(tasks))
            .catch(() => res.sendStatus(500));
    });

    router.put('/:id', auth, async (req, res) => {
        const newData = req.body;
        const taskData = await  Task.findOne({_id: req.params.id});

        for (let key in newData) {
            taskData[key] = newData[key];
        }

        taskData.save()
            .then(task => res.send(task))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', auth, (req, res) => {
        Task.deleteOne({_id: req.params.id})
            .then(task => res.send(task))
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;